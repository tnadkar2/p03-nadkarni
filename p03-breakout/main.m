//
//  main.m
//  p03-breakout
//
//  Created by Tejas Nadkarni on 13/02/16.
//  Copyright © 2016 Tejas Nadkarni. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
