//
//  BreakoutViewController.h
//  p03-breakout
//
//  Created by Tejas Nadkarni on 13/02/16.
//  Copyright © 2016 Tejas Nadkarni. All rights reserved.
//

#import <UIKit/UIKit.h>
NSInteger scoreTotal;

@interface BreakoutViewController : UIViewController
{
    float dx, dy;  // Ball motion
}

@property (nonatomic, strong) UIView *paddle;
@property (nonatomic, strong) UIView *ball;
@property (nonatomic, strong) UIView *strip;
@property (nonatomic, strong) NSTimer *timer;

@property (nonatomic, strong) NSMutableArray *bricks;


@end
