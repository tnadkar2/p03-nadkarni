//
//  BreakoutViewController.m
//  p03-breakout
//
//  Created by Tejas Nadkarni on 13/02/16.
//  Copyright © 2016 Tejas Nadkarni. All rights reserved.
//

#import "BreakoutViewController.h"

@interface BreakoutViewController ()
@end

@implementation BreakoutViewController



@synthesize paddle, ball;
@synthesize strip;
@synthesize timer;
@synthesize bricks;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.jpg"]];
    // Do any additional setup after loading the view.
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenWidth = screenSize.width;
    CGFloat screenHeight = screenSize.height;
    
    paddle = [[UIView alloc] initWithFrame:CGRectMake(20, screenHeight-30, 70, 13)];
    [self.view addSubview:paddle];
    [paddle setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"paddle.jpg"]]];
    
    
    ball = [[UIView alloc]initWithFrame:CGRectMake(100, 100, 15, 15)];
    ball.layer.borderWidth = 2.0f;
    ball.layer.borderColor = [UIColor redColor].CGColor;
    ball.layer.backgroundColor =[UIColor redColor].CGColor;
    ball.layer.cornerRadius = ball.frame.size.width/2.0;
    ball.layer.masksToBounds = YES;
    [self.view addSubview: ball];
    
    
    strip = [[UIView alloc] initWithFrame:CGRectMake(0, screenHeight-5, screenWidth, 20)];
    [self.view addSubview: strip];
    
    [self initGame];
    
    dx = 10;
    dy = 10;
    
    scoreTotal = 0;
}

- (void) initGame {
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenWidth = screenSize.width;
    
    bricks = [NSMutableArray array];
    
    int y = 40;
    int height = 15;
    for(int k=0; k<5; k ++){
        
        for (int i=0;i<7;i++){
            UILabel *brick;
            int x = (screenWidth/7)*i;
            brick= [[UILabel alloc] initWithFrame:CGRectMake(x, y, screenWidth/7, height)];
            [brick setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"brick.jpg"]]];
            brick.layer.borderColor = [UIColor blackColor].CGColor;
            
            brick.layer.borderWidth = 2;
            [self.view addSubview:brick];
            [bricks addObject:brick];
        }
        y = y + height;
    }
    
    timer = [NSTimer scheduledTimerWithTimeInterval:.05	target:self selector:@selector(timerEvent:) userInfo:nil repeats:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenHeight = screenSize.height;
    for (UITouch *t in touches)
    {
        CGPoint p = [t locationInView:self.view];
        p.y= screenHeight-30;
        [paddle setCenter:p];
        
    }
}

-(void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self touchesBegan:touches withEvent:event];
}

-(void)timerEvent:(id)sender
{
    CGRect bounds = [self.view bounds];
    
    // NSLog(@"Timer event.");
    CGPoint p = [ball center];
    
    if ((p.x + dx) < 0)
        dx = -dx;
    
    if ((p.y + dy) < 0)
        dy = -dy;
    
    if ((p.x + dx) > bounds.size.width)
        dx = -dx;
    
    if ((p.y + dy) > bounds.size.height)
        dy = -dy;
        
    
    p.x += dx;
    p.y += dy;
    [ball setCenter:p];
    
    // Now check to see if we intersect with paddle.  If the movement
    // has placed the ball inside the paddle, we reverse that motion
    // in the Y direction.
    if (CGRectIntersectsRect([ball frame], [paddle frame]))
    {
        dy = -dy;
        p.y += 2*dy;
        [ball setCenter:p];
        
    }
    
    if (CGRectIntersectsRect([ball frame], [strip frame ]))
    {
        [timer invalidate];
        [self showOverMessage];
    }
    
    NSUInteger arraySize = [bricks count];
    for(int j=0; j<arraySize; j++){
        UILabel *x =[bricks objectAtIndex:j];
        if (CGRectIntersectsRect([ball frame], [x frame]))
        {
            if(!x.isHidden){
                //YourClass found!!
                dy = -dy;
                x.hidden = YES;
                scoreTotal = scoreTotal + 10;
            }
        }
    }
    
    int bricksCount = 0;
    for(int k=0;k<arraySize;k++){
        UILabel *x =[bricks objectAtIndex:k];
        if(x.isHidden){
            bricksCount++;
        }
    }
    if(bricksCount == arraySize)
    {
        [timer invalidate];
        [self showWinMessage];
    }
}

- (void) resetGame {
    NSUInteger arraySize = [bricks count];
    for(int k=0;k<arraySize;k++){
        UILabel *x =[bricks objectAtIndex:k];
        if(x.isHidden){
            x.hidden = NO;
            scoreTotal = 0;
        }
    }
    [ball  setCenter:CGPointMake(320.0,240.0)];
    
    
    timer = [NSTimer scheduledTimerWithTimeInterval:.05	target:self selector:@selector(timerEvent:) userInfo:nil repeats:YES];
    
}

- (void) showWinMessage {
    NSString *text = [NSString stringWithFormat: @"YOUR SCORE IS %d", scoreTotal];
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"You Won!"
                                                      message: text
                                                     delegate:self
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
    
    [message show];
}

- (void) showOverMessage {
    NSString *text = [NSString stringWithFormat: @"YOUR SCORE IS %d", scoreTotal];
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"GAME OVER!"
                                                      message: text
                                                     delegate:self
                                            cancelButtonTitle:@"Restart"
                                            otherButtonTitles:nil];
    
    [message show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [self resetGame];
}

@end
