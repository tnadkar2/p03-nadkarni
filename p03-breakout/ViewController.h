//
//  ViewController.h
//  p03-breakout
//
//  Created by Tejas Nadkarni on 13/02/16.
//  Copyright © 2016 Tejas Nadkarni. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIButton *exit;

@end

